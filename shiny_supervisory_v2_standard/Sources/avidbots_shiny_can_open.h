/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2015, Avidbots Corp.
 * @name	comm_task_main
 * @brief	Header file that defines the Avidbots shiny can_open functions
 * @author	Pablo Molina
 */


#ifndef AVIDBOTS_SHINY_CAN_OPEN_H_
#define AVIDBOTS_SHINY_CAN_OPEN_H_

#include "PE_Types.h"

extern void ParseCANOpenMsg(uint8_t* can_msg, uint8_t* data_msg, uint8_t* od_index_b1, uint8_t* od_index_b2, uint8_t* sub_index);

extern void ProcessMainBrushSetCmd(uint8_t* data);

extern void ProcessSideBrushSetCmd(uint8_t* data);

extern void ProcessVacuumSetCmd(uint8_t* data);

extern void ProcessCleaningHeadSetCmd(uint8_t* data);

extern void ProcessCleaningModeSetCmd(uint8_t* data);

extern void ProcessPumpSpeedSetCmd(uint8_t* data);

extern void ProcessStatusVariableGetCmd(uint8_t* data);

extern void ProcessSafetyMonitorSetCmd(uint8_t* data);  //YN added June 30

extern void ProcessEstopReleaseCmd(uint8_t* data);


#endif /* AVIDBOTS_SHINY_CAN_OPEN_H_ */
