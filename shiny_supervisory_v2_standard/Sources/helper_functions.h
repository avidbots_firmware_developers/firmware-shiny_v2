/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2015, Avidbots Corp.
 * @name	helper_functions
 * @brief	Header file that defines the helper functions
 * @author	Pablo Molina
 */

#ifndef HELPER_FUNCTIONS_H_
#define HELPER_FUNCTIONS_H_
#include <stdint.h>
#include "psptypes.h"
#include "mqx_tasks.h"

#define MS_PER_SEC 1000

void sleep_ms(uint16_t time_in_ms);

// Helper function

uint32_t compute_ob_from_indexes(uint8_t ob1, uint8_t ob2, uint8_t sub_index);

#endif /* HELPER_FUNCTIONS_H_ */
