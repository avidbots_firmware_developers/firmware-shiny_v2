/*
 * safety_task_main.c
 *
 *  Created on: Jul 24, 2015
 *      Author: yaran
 */


/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2015, Avidbots Corp.
 * @name	safety_task_main
 * @brief	Source file that defines the safety task main function
 * @author	Pablo Molina
 */

#include "Cpu.h"
#include "Events.h"
#include "mqx_tasks.h"

// LOCAL
#include "safety_task_main.h"


void safety_task_main()
{

	bool local_estop_release= 0;
	PTE_Init();
	TU3_Init(NULL);

	while(1) {
	//	Safety task heartbeat

	  if(g_isr_safety_flag == TRUE)
	  {
		  g_isr_safety_flag = FALSE;
		  Board_LED_1_NegVal();
	  }

	//	Get emergency line status
	  local_estop_release = EStop_In_GetVal();     //get emergency line status
	  _mutex_lock(&g_t_mutex);
	  g_t_estop_release = local_estop_release;
	  _mutex_unlock(&g_t_mutex);

	  if( local_estop_release == FALSE)
	  {
		  printf("Emergency stop!\n");
	//  if there is emergency stop
		  _mutex_lock(&g_t_mutex);
		  g_t_estop_status = E_STOP_ESTOP_LINE;
		  _mutex_unlock(&g_t_mutex);
    // 	Stop stop brush
		  B2_SBrush_ClrVal();
	// 	Stop stop brush
		  B3_Vacuum_ClrVal();
		  CH_A_ClrVal();
		  CH_B_ClrVal();
	//  Enable brakes
		  Brakes_Ctrl_ClrVal();
		  Lsds2_ClrVal();
	  }
	 sleep_ms(10);
	}
}
