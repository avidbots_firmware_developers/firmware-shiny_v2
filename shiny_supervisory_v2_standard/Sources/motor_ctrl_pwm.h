/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2015, Avidbots Corp.
 * @name	motor_ctrl_pwm
 * @brief	Header file that defines the motor control PWM functions
 * @author	Pablo Molina
 */

#ifndef MOTOR_CTRL_PWM_H_
#define MOTOR_CTRL_PWM_H_

#include <stdint.h>
#include "PE_Types.h"

extern LDD_TDeviceData* PWM1_device_data_ptr;

extern void InitBrush();

extern void InitPWMPump();

extern void InitPWMCH();

extern void SetPWMPumpValue(uint8_t value);


#endif /* MOTOR_CTRL_PWM_H_ */
