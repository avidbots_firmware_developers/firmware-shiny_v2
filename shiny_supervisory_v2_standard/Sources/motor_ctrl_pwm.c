/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2015, Avidbots Corp.
 * @name	motor_crtl_pwm
 * @brief	Source file that defines the motor control PWM functions
 * @author	Pablo Molina
 */
#include "helper_functions.h"
#include "motor_ctrl_pwm.h"
#include <limits.h>

// Defining the global variables

LDD_TDeviceData* PWM1_device_data_ptr;
LDD_TDeviceData* PWM2_device_data_ptr;

/**
  * @name 	InitBrush
  * @brief	Initializes Main Brush and Side Brush
 */
void InitBrush()
{
	B2_SBrush_PutVal(FALSE);
}
/**
  * @name 	InitPWMPump
  * @brief	Initializes and enables PWMPump
 */
void InitPWMPump()
{
	uint8_t initial_pwm_pump_a_speed = 0x00;
	PWM1_device_data_ptr = PWM_Pump_A_Init(NULL);
	//	Initialize the speed to be 0
	SetPWMPumpValue(initial_pwm_pump_a_speed);
	// 	sets PWM Pump B to zero
	PWM_Pump_B_SetVal();
	sleep_ms(100);

}


/**
  * @name 	InitPWMCH
  * @brief	Initializes Cleaning head
 */
void InitPWMCH()
{

	CH_A_PutVal(FALSE);
	CH_B_PutVal(FALSE);

}

/**
  * @name 	SetPWMPumpValue
  * @brief	Sets the Pump Speed value 0 - 0% ; 256 - 100%
 */
void SetPWMPumpValue(uint8_t value)
{
  PWM_Pump_A_SetRatio8(PWM1_device_data_ptr, value);
}






