/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2015, Avidbots Corp.
 * @name	state_task_main
 * @brief	Header file that defines the state task main function
 * @author	Pablo Molina
 */

#ifndef STATE_TASK_MAIN_H_
#define STATE_TASK_MAIN_H_


// 	Variables used by this task alone
extern bool ind_red_led_status;
extern bool ind_green_led_status;

void state_task_main();
void update_main_led(uint8_t main_led_status);
#endif /* STATE_TASK_MAIN_H_ */
