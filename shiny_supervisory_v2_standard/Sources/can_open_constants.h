/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2015, Avidbots Corp.
 * @name	can_open_constants
 * @brief	Header file that defines the can_open constants
 * @author	Pablo Molina
 */

#ifndef CAN_OPEN_CONSTANTS_H_
#define CAN_OPEN_CONSTANTS_H_

#define kSDOSend					0x0580
#define kSDOReceive 				0x0600
#define kNMTHeartBeat 			  	0x0700
#define kSDOReadMessageByte1  		0x4F
#define kSDOWriteMessageByte1 		0x2F
#define kSDOCancelByte        		0x80
#define kSDOAbortCSS				0x81

//	CAN open status
#define CAN_OPEN_OK 				0
#define CAN_OPEN_READ_TIMEOUT 		1

#define CAN_ID						0xD
#define READ_FRAME_SIZE				0x5

//#define PC_SAFETY_MONITOR
#endif /* CAN_OPEN_CONSTANTS_H_ */
