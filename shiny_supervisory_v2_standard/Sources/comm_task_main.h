/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2015, Avidbots Corp.
 * @name	comm_task_main
 * @brief	Header file that defines the comm task main function
 * @author	Pablo Molina
 */
#ifndef SOURCES_COMM_TASK_MAIN_H_
#define SOURCES_COMM_TASK_MAIN_H_

#include "global_variables.h"

void comm_task_main();

#endif /* SOURCES_COMM_TASK_MAIN_H_ */
