/* ###################################################################
**     Filename    : mqx_tasks.h
**     Project     : shiny_supervisory_v2_standard
**     Processor   : MK20DX256ZVLL10
**     Component   : Events
**     Version     : Driver 01.00
**     Compiler    : GNU C Compiler
**     Date/Time   : 2015-07-24, 09:31, # CodeGen: 1
**     Abstract    :
**         This is user's event module.
**         Put your event handler code here.
**     Contents    :
**         safety_task - void safety_task(uint32_t task_init_data);
**
** ###################################################################*/
/*!
** @file mqx_tasks.h
** @version 01.00
** @brief
**         This is user's event module.
**         Put your event handler code here.
*/         
/*!
**  @addtogroup mqx_tasks_module mqx_tasks module documentation
**  @{
*/         

#ifndef __mqx_tasks_H
#define __mqx_tasks_H
/* MODULE mqx_tasks */

#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "MQX1.h"
#include "SystemTimer1.h"
#include "Board_LED_1.h"
#include "BitIoLdd1.h"
#include "PTE.h"
#include "CAN1.h"
#include "TU1.h"
#include "PWM_Pump_A.h"
#include "TD1.h"
#include "TU2.h"
#include "TU3.h"
#include "TU4.h"
#include "Board_LED_2.h"
#include "BitIoLdd2.h"
#include "PWM_Pump_B.h"
#include "BitIoLdd3.h"
#include "CH_A.h"
#include "BitIoLdd4.h"
#include "CH_B.h"
#include "BitIoLdd5.h"
#include "B2_SBrush.h"
#include "BitIoLdd6.h"
#include "B3_Vacuum.h"
#include "BitIoLdd7.h"
#include "Power_Button_In.h"
#include "ExtIntLdd2.h"
#include "Contactor_Ctrl.h"
#include "BitIoLdd8.h"
#include "Lsds2.h"
#include "BitIoLdd9.h"
#include "Panel_Ind_R.h"
#include "BitIoLdd10.h"
#include "Panel_Ind_G.h"
#include "BitIoLdd11.h"
#include "Panel_Pwr_LED.h"
#include "BitIoLdd12.h"
#include "TU5.h"
#include "WarningTimer.h"
#include "Panel_Drive_Sw.h"
#include "BitIoLdd13.h"
#include "CsIO1.h"
#include "IO1.h"
#include "Brakes_Ctrl.h"
#include "BitIoLdd14.h"
#include "Lift_measure.h"
#include "AdcLdd1.h"
#include "EStop_In.h"
#include "BitIoLdd15.h"
#include "Left_LED.h"
#include "PwmLdd2.h"
#include "R_StatusStrip.h"
#include "PwmLdd3.h"
#include "G_StatusStrip.h"
#include "PwmLdd4.h"
#include "B_StatusStrip.h"
#include "PwmLdd5.h"
#include "CH_EN.h"
#include "BitIoLdd16.h"
#include "Pump_EN.h"
#include "BitIoLdd17.h"
#include "PwmLdd1.h"
#include "TU6.h"
#include "Right_LED.h"

#include "global_variables.h"
#include "safety_task_main.h"
#include "comm_task_main.h"
#include "state_task_main.h"
#include "control_task_main.h"
#ifdef __cplusplus
extern "C" {
#endif 

/*
** ===================================================================
**     Event       :  safety_task (module mqx_tasks)
**
**     Component   :  Task1 [MQXLite_task]
**     Description :
**         MQX task routine. The routine is generated into mqx_tasks.c
**         file.
**     Parameters  :
**         NAME            - DESCRIPTION
**         task_init_data  - 
**     Returns     : Nothing
** ===================================================================
*/
void safety_task(uint32_t task_init_data);


/*
** ===================================================================
**     Event       :  comm_task (module mqx_tasks)
**
**     Component   :  Task2 [MQXLite_task]
**     Description :
**         MQX task routine. The routine is generated into mqx_tasks.c
**         file.
**     Parameters  :
**         NAME            - DESCRIPTION
**         task_init_data  - 
**     Returns     : Nothing
** ===================================================================
*/
void comm_task(uint32_t task_init_data);

/*
** ===================================================================
**     Event       :  state_task (module mqx_tasks)
**
**     Component   :  Task3 [MQXLite_task]
**     Description :
**         MQX task routine. The routine is generated into mqx_tasks.c
**         file.
**     Parameters  :
**         NAME            - DESCRIPTION
**         task_init_data  - 
**     Returns     : Nothing
** ===================================================================
*/
void state_task(uint32_t task_init_data);

/*
** ===================================================================
**     Event       :  control_task (module mqx_tasks)
**
**     Component   :  Task4 [MQXLite_task]
**     Description :
**         MQX task routine. The routine is generated into mqx_tasks.c
**         file.
**     Parameters  :
**         NAME            - DESCRIPTION
**         task_init_data  - 
**     Returns     : Nothing
** ===================================================================
*/
void control_task(uint32_t task_init_data);

/* END mqx_tasks */

#ifdef __cplusplus
}  /* extern "C" */
#endif 

#endif 
/* ifndef __mqx_tasks_H*/
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.5 [05.21]
**     for the Freescale Kinetis series of microcontrollers.
**
** ###################################################################
*/
