/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2015, Avidbots Corp.
 * @name	global_variables
 * @brief	Source file that defines global variables
 * @author	Pablo Molina
 */

#include "global_variables.h"
#include "mutex.h"

// 	Defining the task mutex
MUTEX_STRUCT 	g_t_mutex;

//	Defining the global variables

//	These variables contain estop status and reason
uint8_t g_t_estop_status = E_STOP_OK;
bool 	g_t_estop_release = FALSE;
bool 	g_isr_safety_flag = FALSE;
bool 	g_t_estop_state = TRUE;

//	These variables contain can
bool 	g_isr_comm_Heartbeat = FALSE;
bool 	g_isr_Can1Rx_Flag = FALSE;

uint8_t g_t_state_variable = MCU_STATE_VAR_OFF;
bool 	g_t_state_driving_status = FALSE;

//	Power Button Input
volatile bool g_isr_Pwr_Butt_Status = TRUE;

//	Timer flag for all of LEDS
bool 	g_isr_Green_Timer_Flag = 0;
bool 	g_isr_Red_Timer_Flag = 0;
bool 	g_isr_Yellow_Timer_Flag = 0;
bool 	g_isr_Button_Timer_Flag = 0;
bool 	g_isr_Warning_30_Sec = 0;
bool	g_isr_obstacle_strip_flag = 0;
bool 	g_isr_emergency_strip_flag = 0;
bool 	g_isr_warning_strip_flag = 0;
bool	g_isr_glow_flag = 0;

//	Time variables for safety monitor
uint16_t timeMS = 0;
uint16_t prev_timeMS = 0;
LDD_TimeDate_TTimeRec current_time;

// 	This variable contains the safety monitor status
uint8_t g_t_SafetyCmd = SAFETY_MONITOR_NO_CMD;

// 	This variable contains the pump speed
uint8_t pump_speed = 0;

// 	This variable controls the status of the main brush - true is ON
bool 	main_brush_status = FALSE;

// 	This variable controls the status of the main vacuum - true is ON
bool 	vacuum_status = FALSE;

// 	This variable controls the status of the side brush - true is ON
bool 	side_brush_status = FALSE;

// 	This variable controls the status of the cleaning head - true is UP
uint8_t	cleaning_head_status = FALSE;

uint8_t g_t_motor_position_max;
uint8_t g_t_motor_position_min;
uint8_t g_t_motor_position;
bool g_t_Lift_measure_flag = TRUE;
//	This varaible controls the mode of cleaning - true is auto
bool 	mode_status = FALSE;
#ifdef PC_SAFETY_MONITOR
bool 			g_t_pc_status = TRUE;  // True means not ready and false means ready
#endif
#ifndef PC_SAFETY_MONITOR
bool g_t_pc_status = FALSE;
#endif
// This variable holds the status of manual driving - FALSE = no driving, TRUE = drive
bool 	 		g_t_driving_sw_status = FALSE;
