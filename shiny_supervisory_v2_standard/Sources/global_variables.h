/*
 * global_variables.h
 *
 *  Created on: Jul 24, 2015
 *      Author: yaran
 */


#ifndef SOURCES_GLOBAL_VARIABLES_H_
#define SOURCES_GLOBAL_VARIABLES_H_
/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2015, Avidbots Corp.
 * @name	global_variables
 * @brief	Header file that defines global variables
 * @author	Pablo Molina
 */
#include "PE_Types.h"
#include "psptypes.h"
#include "queue.h"
#include "mutex.h"
#include "CAN1.h"

// 	STATE VARIABLE POSIBLE STATES
#define MCU_STATE_VAR_OFF 				0
#define	MCU_STATE_VAR_LOADING			1
#define	MCU_STATE_VAR_MANUAL			2
#define	MCU_STATE_VAR_AUTO				3
#define	MCU_STATE_VAR_EMERGENCY   		4
#define MCU_STATE_VAR_WARNING_OFF		5
#define MCU_STATE_VAR_AUTO_OBSTACLE		6

// 	ESTOP VARIABLE POSSIBLE REASONS
#define E_STOP_ESTOP_LINE				0
#define E_STOP_IR_SENSORS				1
#define E_STOP_OK						2
#define E_STOP_HANDSHAKE_PC				3
#define E_STOP_SAFETY_ZONE				4
//	SAFETY MONITOR POSSIBLE STATUS
//#define PC_SAFETY_MONITOR
#define	SAFETY_MONITOR_NORMAL			0
#define SAFETY_MONITOR_FAILED			1
#define SAFETY_MONITOR_NO_CMD			2

//	Cleaning head position

#define MOTOR_POSITION_TOLERANCE 1
#define MOTOR_POSITION_1	150
#define MOTOR_POSITION_2	100
#define MOTOR_POSITION_3	50
#define	MOTOR_POSITION_4	25
#define MOTOT_POSITION_5	10
#define MOTOR_POSITION_6	5 //Motor position should not be lower than 1

// 	Mutex variables
extern MUTEX_STRUCT g_t_mutex;

//	State vairbales
extern uint8_t 	g_t_state_variable;
extern bool g_t_state_driving_status;
extern bool g_t_driving_sw_status;

//	COMMUNICATION HEARTBEAT STATUS VARIABLE
extern bool g_isr_comm_Heartbeat;
//	can receiver buffer flag
extern bool g_isr_Can1Rx_Flag;
//	estop variables
extern bool g_t_estop_release;
extern bool g_t_estop_state;
extern bool g_isr_safety_flag;
extern uint8_t 	g_t_estop_status;

//	Power Button Status Variable
extern volatile bool g_isr_Pwr_Butt_Status;
extern 	LDD_TDeviceData *RTPtr;
// 	leds timers flags
extern bool g_isr_Red_Timer_Flag;
extern bool g_isr_Green_Timer_Flag;
extern bool g_isr_Yellow_Timer_Flag;
extern bool g_isr_Button_Timer_Flag;
extern bool g_isr_Warning_30_Sec;
extern bool g_isr_obstacle_strip_flag;
extern bool g_isr_emergency_strip_flag;
extern bool g_isr_warning_strip_flag;
extern bool g_isr_glow_flag;
//	time variables of safety monitor
extern uint16_t timeMS;
extern uint16_t prev_timeMS;
extern LDD_TimeDate_TTimeRec current_time;
extern bool g_t_pc_status;
extern uint8_t g_t_SafetyCmd;
// CLEANING STATUS VARIABLES
extern uint8_t pump_speed;
extern bool main_brush_status;
extern bool vacuum_status;
extern bool side_brush_status;
extern uint8_t cleaning_head_status;
extern bool mode_status;
extern uint8_t g_t_motor_position;
extern uint8_t g_t_motor_position_max;
extern uint8_t g_t_motor_position_min;
extern bool g_t_Lift_measure_flag;
#endif /* SOURCES_GLOBAL_VARIABLES_H_ */
