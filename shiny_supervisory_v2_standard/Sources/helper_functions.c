/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2015, Avidbots Corp.
 * @name	helper_functions
 * @brief	Source file that defines the helper functions
 * @author	Pablo Molina
 */

#include "helper_functions.h"

/**
  * @name 	sleep_ms
  * @brief	delay amount of time in ms
  * @[param]In: time_in_ms
 */

void sleep_ms(uint16_t time_in_ms)
{
	float time_ticks;
	_mqx_uint ticks_per_second 		= _time_get_ticks_per_sec();

	time_ticks = ((float)time_in_ms / (float)MS_PER_SEC) * (float)(ticks_per_second);

	_time_delay_ticks((uint32_t)(time_ticks));

}

/**
  * @name 	compute_ob_from_indexes
  * @brief	Computer into entire frame
  * @[param]In: ob1
  * 			ob2
  * 			sub_index
  * @[param]Out: ob1 << 16 | ob2 << 8 | sub_index
 */

uint32_t compute_ob_from_indexes(uint8_t ob1, uint8_t ob2, uint8_t sub_index)
{
	return (ob1 << 16 | ob2 << 8 | sub_index);
}
