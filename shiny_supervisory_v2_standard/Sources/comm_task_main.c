/*
 * comm_task_main.c
 *
 *  Created on: Jul 24, 2015
 *      Author: yaran
 */

#include "comm_task_main.h"

/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2015, Avidbots Corp.
 * @name	comm_task_main
 * @brief	Source file that defines the comm task main function
 * @author	Pablo Molina
 */
#include <string.h>

#include <stdio.h>
#include <stdlib.h>
#include "comm_task_main.h"
#include "Cpu.h"
#include "Events.h"
#include "mqx_tasks.h"
#include "can_open.h"
#include "helper_functions.h"
#include "can_open_constants.h"
#include "avidbots_shiny_can_open.h"
#include "avidbots_shiny_can_open_constants.h"
#include "motor_ctrl_pwm.h"
#include "global_variables.h"

/**
  * @name 	comm_task_main()
  * @brief	This function receive, parse, and reply CAN frame
 */

void comm_task_main()
{
	//	Initialize variables used throughout the main comm loop

	uint8_t can_msg[5] 	= {0,0,0,0,0};
	uint8_t data_msg = 0;
	//	CANOpen Object index buffer- 1st byte
	uint8_t od_index_b1;
	//	CANOpen Object index buffer- 2nd byte
	uint8_t od_index_b2;
	//	CANOpen sub index
	uint8_t sub_index;

	//	For ring buffer
/*	uint32_t size = 0;
	uint32_t oklen = 0;
	struct fifo *fifo_buf= NULL;

	size = 6 * sizeof(LDD_CAN_TFrame);
	fifo_buf = fifo_alloc(size);
*/
	LDD_TimeDate_TTimeRec current_time;
	LDD_TimeDate_TTimeRec prev_time;
	uint16_t time_diff = 0;

	//	Set CAN ID
	uint8_t Can_Id = CAN_ID;
	uint32_t ob_index = 0;

	//	Initialize CAN1 device
	InitCAN1();
	//	Initialize the (Time out) timer for CAN1
	InitCAN1Timer();
	//	Initialize PWM channel for Pump
	InitPWMPump();
	//	Initialize Cleaning head
	InitPWMCH();
	//	Initialize Main and side brush
	InitBrush();
	//	Initialize heartbeat timer
	TU4_Init(NULL);

	// 	Setting up CAN variables
	LDD_CAN_TMBIndex RxBufferIdx = 0x0U;
	LDD_CAN_TFrame RxFrames;
	LDD_TError Type = ERR_FAILED;
	RxFrames.FrameType = LDD_CAN_DATA_FRAME;
	uint8_t RxFrameData[5] = {0,0,0,0,0};

	RxFrames.Data = RxFrameData; //TO MODIFY
	LDD_CAN_TMessageID MessageID = kSDOReceive + CAN_ID ;
	CAN1_SetRxBufferID(CAN1_device_data_ptr, RxBufferIdx, MessageID);
	current_time.Hour = 0;
	current_time.Min = 0;
	current_time.Sec = 0;
	current_time.Sec100= 0;
	//	Get current time
	TD1_SetTime(CANTimer_device_data_ptr, &current_time);

	while(1) {
	//	Check communication heartbeat
		if(g_isr_comm_Heartbeat == TRUE)
		{
	//	Send empty heartbeat CAN message
			SendHeartbeatCAN1(Can_Id);
	//  Blink LED 2
			Board_LED_2_NegVal();
	//	Clear heartbeat status
			g_isr_comm_Heartbeat = FALSE;
		}
		if(g_isr_Can1Rx_Flag)
		{
	//	Read frame
			Type = CAN1_ReadFrame(CAN1_device_data_ptr, RxBufferIdx, &RxFrames);  //to do
			g_isr_Can1Rx_Flag = 0;

			if(Type == ERR_OK)
			{
				for (uint8_t i = 0; i < RxFrames.Length ; i++ )

				{
					can_msg[i] = RxFrames.Data[i];
				}
				ParseCANOpenMsg(can_msg, &data_msg, &od_index_b1, &od_index_b2, &sub_index);
			}
				ob_index = compute_ob_from_indexes(od_index_b1, od_index_b2,sub_index);

	//	Make right decision based on CAN command message
			switch(ob_index)
			{
				case kSideBrushCmdOB:
					 ProcessSideBrushSetCmd(&data_msg);
					 break;
				case kVacuumCmdOB:
					 ProcessVacuumSetCmd(&data_msg);
					 break;
				case kCleaningHeadCmdOB:
					 ProcessCleaningHeadSetCmd(&data_msg);
					 break;
				case kCleaningModeCmdOB:
					 ProcessCleaningModeSetCmd(&data_msg);
					 break;
				case kPumpSpeedCmdOB:
					 ProcessPumpSpeedSetCmd(&data_msg);
					 break;
				case kSafetyMonitorSetCmdOB:
					 ProcessSafetyMonitorSetCmd(&data_msg);
					 break;
				case kReadStatusVariableOB:
					 ProcessStatusVariableGetCmd(&data_msg);
					 break;
				case kEstopSetCmdOB:
					 ProcessEstopReleaseCmd(&data_msg);
					 break;
				default:
					 SendUnknowcmdCAN1(Can_Id,can_msg);

			}
	}
	//	check saftey monitor status
#ifdef PC_SAFETY_MONITOR
	//	command received
	if((g_t_SafetyCmd == SAFETY_MONITOR_FAILED) || (g_t_SafetyCmd == SAFETY_MONITOR_NORMAL))
		{
			if(timeMS == prev_timeMS)
			{
				TD1_GetTime(CANTimer_device_data_ptr, &current_time);
				time_diff = (current_time.Hour *360000 	+ current_time.Min * 6000 + current_time.Sec*100 + current_time.Sec100)-prev_timeMS;
	//	if greater than 200ms
				if (time_diff > 20)
	//	pc is not ready
					g_t_pc_status = SAFETY_MONITOR_FAILED;
			}
			else
				g_t_pc_status = g_t_SafetyCmd;
		}
		else
		{
	//	pc hasnt receive command yet
			g_t_pc_status = SAFETY_MONITOR_FAILED;
		}
		prev_timeMS = timeMS;
#endif
	//	rest
		sleep_ms(6);
	}
}






