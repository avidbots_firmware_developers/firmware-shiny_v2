/*
`-+ *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2015, Avidbots Corp.
 * @name	state_task_main
 * @brief	Source file that defines the state task main function
 * @author	Pablo Molina
 */


#include "Cpu.h"
#include "Events.h"
#include "mqx_tasks.h"

// LOCAL
#include "state_task_main.h"
#include "global_variables.h"
#include "helper_functions.h"
#include  <string.h>

/**
  * @name 	state_task_main()
  * @brief	This function changes and updates states
 */



void state_task_main()
{
	//	Creating the local variables of this task
	uint8_t local_state_variable 	= MCU_STATE_VAR_OFF;
	uint8_t prev_state_variable		= MCU_STATE_VAR_MANUAL;
	volatile bool local_pwr_status = TRUE;

	//	TRUE is ready, False is not ready
	uint8_t state_task_estop = FALSE;
	bool state_estopCmd =  TRUE;
	bool local_pc_status = SAFETY_MONITOR_NO_CMD;
	bool local_drive_switch = TRUE;
	bool local_driving_cmd = FALSE;
	Contactor_Ctrl_ClrVal();
	LDD_TDeviceData* Timer_Data_Ptr = TU5_Init(NULL);
	LDD_TDeviceData* WarningTimer_Ptr = WarningTimer_Init(NULL);
	while(1)
	{
	//	lock global variable and read into local
		_mutex_lock(&g_t_mutex);
		local_driving_cmd = g_t_state_driving_status;
		state_task_estop = g_t_estop_release;
		g_t_state_variable = local_state_variable;
		local_drive_switch = !Panel_Drive_Sw_GetVal();
		g_t_driving_sw_status = local_drive_switch;
		local_pc_status = g_t_pc_status;
		state_estopCmd = g_t_estop_state;
		_mutex_unlock(&g_t_mutex);
	//	Disable push button hardware interrupt
		Power_Button_In_Disable();
	// 	Read and update push button status
		local_pwr_status = g_isr_Pwr_Butt_Status;
		g_isr_Pwr_Butt_Status = TRUE;
	//	Enable Interrupt again
		Power_Button_In_Enable();
	//	State transition
		switch(local_state_variable)
		{
	//	Machine is off
			case MCU_STATE_VAR_OFF:
	//	Power button LED off
				 Panel_Pwr_LED_ClrVal();
	//	Main contactor off
				 Contactor_Ctrl_ClrVal();
	//	Safety relay off
				 Lsds2_ClrVal();
	//	Engage brakes
				 Brakes_Ctrl_ClrVal();
	//	Update LED indicator
				 update_main_led(local_state_variable);
	//	Update LED strip
			//	 update_LedStrip(local_state_variable);
	// 	State transition

	//	If push button on
				 if (local_pwr_status == FALSE)
				 {
	//	Power button LED on
					Panel_Pwr_LED_SetVal();
	//	Turn on main contactor
					Contactor_Ctrl_SetVal();
	//	Transit to loading state
					local_state_variable =  MCU_STATE_VAR_LOADING;
					local_pwr_status = TRUE;
				 }
				 else
	//	Otherwise stay off
					local_state_variable = MCU_STATE_VAR_OFF;
				 break;
			case MCU_STATE_VAR_WARNING_OFF:
	// 	Give warning to turn everything off
	//	update LED indicator
				update_main_led(local_state_variable);
	//	update side led
			//	update_LedStrip(local_state_variable);
	// 	Enable brakes
				Brakes_Ctrl_ClrVal();
	//	Wait 30 sec and transit to off
				 if(g_isr_Warning_30_Sec == TRUE)
				 {
				   local_state_variable =  MCU_STATE_VAR_OFF;  //TO DO add loading state
				   g_isr_Warning_30_Sec = FALSE;
				 }
				 else
	//	Otherwise stay this state
					local_state_variable = MCU_STATE_VAR_WARNING_OFF;
				 break;
    //	waiting for pc ready
			case MCU_STATE_VAR_LOADING:
	//	Update LED indicator
			  update_main_led(local_state_variable);

	//	update side led
			//  update_LedStrip(local_state_variable);


	// 	Turn on Push button LED
			  Panel_Pwr_LED_SetVal();
	//	state transition
	//	turn off robot
			  if (local_pwr_status == FALSE)
			  {
	//	Go to warning state and start counting 30 secs
				  local_state_variable = MCU_STATE_VAR_WARNING_OFF;
				  g_isr_Warning_30_Sec = 0;
				  WarningTimer_ResetCounter(WarningTimer_Ptr);
				  local_pwr_status = TRUE;
			  }
	//	Go to manual mode when everything is ready
			  else if((local_pc_status == FALSE) &&(state_task_estop == TRUE))	//go to manual mode
			  {
				  local_state_variable = MCU_STATE_VAR_MANUAL;
				  local_pwr_status = TRUE;
				  state_estopCmd = TRUE;
			  }
	//	Stay this state
			  else
				  local_state_variable = MCU_STATE_VAR_LOADING;
			  break;
	//	Manual mode
			case MCU_STATE_VAR_MANUAL:
				update_main_led(local_state_variable);
	//	update side led
			//	update_LedStrip(local_state_variable);
	//	power button led on
				Panel_Pwr_LED_SetVal();
	//	+12V ready
				Lsds2_SetVal();
	//	Release break
				Brakes_Ctrl_SetVal();
	// 	state transition
	//	turn off
				if (local_pwr_status == FALSE)
				{
					local_state_variable = MCU_STATE_VAR_WARNING_OFF;
					g_isr_Warning_30_Sec = 0;
					WarningTimer_ResetCounter(WarningTimer_Ptr);
					local_pwr_status = TRUE;
				}
	//	emergency stop
				else if ((state_task_estop == FALSE) || (local_pc_status == TRUE))
				{
					prev_state_variable = local_state_variable;
					local_state_variable = MCU_STATE_VAR_EMERGENCY;
				}
	//	Auto mode
				else if ((local_drive_switch == FALSE) && (local_driving_cmd == TRUE))
				{
					local_state_variable = MCU_STATE_VAR_AUTO;
					local_drive_switch = TRUE;
				}
	//	stay manual
				else
					local_state_variable = MCU_STATE_VAR_MANUAL;
				break;
	//	Auto mode
			case MCU_STATE_VAR_AUTO:
    //  Update LED indicator
				update_main_led(local_state_variable);
	//	update side led
			//	update_LedStrip(local_state_variable);
	//	Power button LED on
				Panel_Pwr_LED_SetVal();
	//	Safety relay on
				Lsds2_SetVal();
	//  Release brakes
				Brakes_Ctrl_SetVal();
	//	State transition

	//	Off state
				if (local_pwr_status == FALSE)
				{
					local_state_variable = MCU_STATE_VAR_WARNING_OFF;
					g_isr_Warning_30_Sec = 0;
					WarningTimer_ResetCounter(WarningTimer_Ptr);
					local_pwr_status = TRUE;
				}
	//	Emergency state
				else if ((state_task_estop == FALSE) ||(local_pc_status == TRUE))
				{
					prev_state_variable = local_state_variable;
					local_state_variable = MCU_STATE_VAR_EMERGENCY;
				}
	//	Manual mode
				else if (local_drive_switch == TRUE)
				{
					local_state_variable = MCU_STATE_VAR_MANUAL;
				}
	//  Keep same state
				else
					local_state_variable = MCU_STATE_VAR_AUTO;
				break;
			case MCU_STATE_VAR_AUTO_OBSTACLE:
			//	update_LedStrip(local_state_variable);
				break;
	//	Emergency state
			case MCU_STATE_VAR_EMERGENCY:
				update_main_led(local_state_variable);
	//	update side led
			//	update_LedStrip(local_state_variable);
				Lsds2_ClrVal();
				Brakes_Ctrl_ClrVal();
	//	State transition
	//	Emergency mode
				if (local_pwr_status == FALSE)
				{
					local_state_variable = MCU_STATE_VAR_WARNING_OFF;
					g_isr_Warning_30_Sec = 0;
					WarningTimer_ResetCounter(WarningTimer_Ptr);
					local_pwr_status = TRUE;
				}
	//	Back to previous mode
				else if((local_pc_status == FALSE) && (state_task_estop == TRUE) && (state_estopCmd == FALSE))
				{
						local_state_variable = prev_state_variable;
						local_pwr_status = TRUE;
						state_estopCmd = TRUE;
						_mutex_lock(&g_t_mutex);
						g_t_estop_state = state_estopCmd;
						_mutex_unlock(&g_t_mutex);
				}
	//	Stay the same state
				else
					local_state_variable = MCU_STATE_VAR_EMERGENCY;

				break;
		}
		sleep_ms(10);
	}

}

/**
  * @name 	update_main_led()
  * @brief	This function update LED indicator on panel
  * param[in]	main_led_status (status of LED)
 */


void update_main_led(uint8_t main_led_status)
{
	//	State transition
	switch(main_led_status)
	{
	//	Off state
		case MCU_STATE_VAR_OFF:
			Panel_Ind_R_ClrVal();
			Panel_Ind_G_ClrVal();
			printf(" All led off\n");
			break;
	//	Prepare to turn off
		case MCU_STATE_VAR_WARNING_OFF:
			if(g_isr_Button_Timer_Flag == 1)
			{
				Panel_Pwr_LED_NegVal();
				g_isr_Button_Timer_Flag = 0;
			}
			printf(" Warning off\n");
			break;
	//	loading pc
		case MCU_STATE_VAR_LOADING:
			Panel_Ind_R_ClrVal();
	//	blink green led
			if(g_isr_Green_Timer_Flag == 1)
			{
				Panel_Ind_G_NegVal();
				printf(" Main led Blink Green\n");
				g_isr_Green_Timer_Flag = 0;
			}
			break;
	//	manual mode
		case MCU_STATE_VAR_MANUAL:
			printf(" Main led Yellow\n");
	//	yellow led solid
			Panel_Ind_G_SetVal();
			Panel_Ind_R_SetVal();
			break;
	//	auto mode
		case MCU_STATE_VAR_AUTO:
			if(g_isr_Yellow_Timer_Flag == 1)
			{
	//	Yellow led blink
				Panel_Ind_G_NegVal();
				Panel_Ind_R_NegVal();
				printf(" Main led Blink Yellow\n");
				g_isr_Yellow_Timer_Flag = 0;
			}
			break;
	//	Emergency mode
		case MCU_STATE_VAR_EMERGENCY:
			Panel_Ind_G_ClrVal();
			printf(" Main led Blink red\n");
	//	Blink red
			if(g_isr_Red_Timer_Flag == 1)
			{
				Panel_Ind_R_NegVal();
				g_isr_Red_Timer_Flag = 0;
			}
			break;
	}
}
