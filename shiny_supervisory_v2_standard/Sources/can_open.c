/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2015, Avidbots Corp.
 * @name	can_open
 * @brief	Source file that contains the can_open functions
 * @author	Pablo Molina
 */


#include "can_open.h"
#include "mqx_tasks.h"
#include "PE_Types.h"
#include "psptypes.h"
#include "can_open_constants.h"


// Global variables of the CAN Open

CAN1_TDeviceData *CAN1_device_data_ptr;
LDD_TDeviceData *CANTimer_device_data_ptr;

/**
  * @name 	InitCAN1Timer
  * @brief	This function initializes the CAN timer
 */
void InitCAN1Timer()
{
	CANTimer_device_data_ptr = TD1_Init(NULL);
	TD1_Enable(CANTimer_device_data_ptr);
}


/**
  * @name 	InitCAN1
  * @brief	This function initializes the CAN1 module on MCU
 */
void InitCAN1()
{
	CAN1_device_data_ptr = CAN1_Init(NULL);
}

/**
  * @name 	SendSDOCanOpenCAN1
  * @brief	This function sends an SDO CanOpen command to CAN1 module on MCU
  * @param[in] 	can_id:  the CAN ID of the message
  * @param[in] 	ccs_byte:  The CCS byte of the message
  * @param[in] 	od_index_b1: The command Object dictionary byte 1
  * @param[in] 	od_index_b2: The command Object dictionary byte 2
  * @param[in] 	od_sub_index: The subindex of the current message
  * @param[in] 	msg_data: The data of the message (PTR)
 */
void SendSDOCanOpenCAN1(uint16_t can_id, uint8_t ccs_byte,
											uint8_t od_index_b1, uint8_t od_index_b2,
											uint8_t od_sub_index, uint8_t* msg_data)
{
	uint8_t data[8] = {0,0,0,0,0};

	//	CAN transmit buffer for use of transmission
	LDD_CAN_TMBIndex TxBufferIdx = 0x1U;
	LDD_CAN_TFrame TxFrames;
	TxFrames.FrameType = LDD_CAN_DATA_FRAME;
	TxFrames.Length = 5;
	TxFrames.MessageID = can_id;
	//	Copy transmitted message into array
	data[0] = ccs_byte;
	data[2] = od_index_b1;
	data[1] = od_index_b2;
	data[3] = od_sub_index;
	//  Copy the data into the buffer
	data[4] = *msg_data;

	TxFrames.Data = data;
	// 	Transmit the message
	CAN1_SendFrame(CAN1_device_data_ptr, TxBufferIdx, &TxFrames);
}

/**
  * @name 	SendHeartbeatCAN1
  * @brief	This function sends an communication heartbeat message through CAN1 module on MCU
  * @param[in] 	can_id:  the CAN ID of the message
  *
 */
void SendHeartbeatCAN1(uint8_t can_id)
{
	LDD_CAN_TMBIndex TxBufferIdx=0x1U;
	LDD_CAN_TFrame TxFrames;
	TxFrames.FrameType = LDD_CAN_DATA_FRAME;
	//	Send an empty frame
	TxFrames.Length = 0;
	TxFrames.MessageID = kNMTHeartBeat  + can_id;
	//	Transmit heartbeat message
	CAN1_SendFrame(CAN1_device_data_ptr,TxBufferIdx, &TxFrames);
}
/**
  * @name 	SendUnknowcmdCAN1
  * @brief	This function sends an message that means the received CAN frame is not recognized
  * @param[in] 	can_id:  the CAN ID of the message
  * @param[in] 	msg_data: The data of the message (PTR)
 */
void SendUnknowcmdCAN1(uint8_t can_id, uint8_t *data)
{
	LDD_CAN_TMBIndex TxBufferIdx=0x1U;
	LDD_CAN_TFrame TxFrames;
	TxFrames.FrameType = LDD_CAN_DATA_FRAME;
	TxFrames.Length = 3;
	TxFrames.MessageID = kSDOSend + can_id;
	data[0] = kSDOAbortCSS;
	TxFrames.Data = data;
	CAN1_SendFrame(CAN1_device_data_ptr,TxBufferIdx, &TxFrames);
}

