/* ###################################################################
**     Filename    : main.c
**     Project     : shiny_supervisory_v2_standard
**     Processor   : MK20DX256ZVLL10
**     Version     : Driver 01.01
**     Compiler    : GNU C Compiler
**     Date/Time   : 2015-07-24, 09:26, # CodeGen: 0
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.01
** @brief
**         Main module.
**         This module contains user's application code.
*/         
/*!
**  @addtogroup main_module main module documentation
**  @{
*/         
/* MODULE main */


/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "Events.h"
#include "mqx_tasks.h"
#include "MQX1.h"
#include "SystemTimer1.h"
#include "Board_LED_1.h"
#include "BitIoLdd1.h"
#include "PTE.h"
#include "CAN1.h"
#include "TU1.h"
#include "PWM_Pump_A.h"
#include "TD1.h"
#include "TU2.h"
#include "TU3.h"
#include "TU4.h"
#include "Board_LED_2.h"
#include "BitIoLdd2.h"
#include "PWM_Pump_B.h"
#include "BitIoLdd3.h"
#include "CH_A.h"
#include "BitIoLdd4.h"
#include "CH_B.h"
#include "BitIoLdd5.h"
#include "B2_SBrush.h"
#include "BitIoLdd6.h"
#include "B3_Vacuum.h"
#include "BitIoLdd7.h"
#include "Power_Button_In.h"
#include "ExtIntLdd2.h"
#include "Contactor_Ctrl.h"
#include "BitIoLdd8.h"
#include "Lsds2.h"
#include "BitIoLdd9.h"
#include "Panel_Ind_R.h"
#include "BitIoLdd10.h"
#include "Panel_Ind_G.h"
#include "BitIoLdd11.h"
#include "Panel_Pwr_LED.h"
#include "BitIoLdd12.h"
#include "TU5.h"
#include "WarningTimer.h"
#include "Panel_Drive_Sw.h"
#include "BitIoLdd13.h"
#include "CsIO1.h"
#include "IO1.h"
#include "Brakes_Ctrl.h"
#include "BitIoLdd14.h"
#include "Lift_measure.h"
#include "AdcLdd1.h"
#include "EStop_In.h"
#include "BitIoLdd15.h"
#include "Left_LED.h"
#include "PwmLdd2.h"
#include "R_StatusStrip.h"
#include "PwmLdd3.h"
#include "G_StatusStrip.h"
#include "PwmLdd4.h"
#include "B_StatusStrip.h"
#include "PwmLdd5.h"
#include "CH_EN.h"
#include "BitIoLdd16.h"
#include "Pump_EN.h"
#include "BitIoLdd17.h"
#include "PwmLdd1.h"
#include "TU6.h"
#include "Right_LED.h"
/* Including shared modules, which are used for whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
/* User includes (#include below this line is not maintained by Processor Expert) */
#include "global_variables.h"
/*lint -save  -e970 Disable MISRA rule (6.3) checking. */
int main(void)
/*lint -restore Enable MISRA rule (6.3) checking. */
{
  /* Write your local variable definition here */

  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  PE_low_level_init();
  /*** End of Processor Expert internal initialization.                    ***/

  /* Write your code here */
  /* For example: for(;;) { } */

  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;){}
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.5 [05.21]
**     for the Freescale Kinetis series of microcontrollers.
**
** ###################################################################
*/
