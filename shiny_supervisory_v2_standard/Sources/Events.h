/* ###################################################################
**     Filename    : Events.h
**     Project     : shiny_supervisory_v2_standard
**     Processor   : MK20DX256ZVLL10
**     Component   : Events
**     Version     : Driver 01.00
**     Compiler    : GNU C Compiler
**     Date/Time   : 2015-07-24, 09:26, # CodeGen: 0
**     Abstract    :
**         This is user's event module.
**         Put your event handler code here.
**     Contents    :
**         Cpu_OnNMIINT - void Cpu_OnNMIINT(void);
**
** ###################################################################*/
/*!
** @file Events.h
** @version 01.00
** @brief
**         This is user's event module.
**         Put your event handler code here.
*/         
/*!
**  @addtogroup Events_module Events module documentation
**  @{
*/         

#ifndef __Events_H
#define __Events_H
/* MODULE Events */

#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "MQX1.h"
#include "SystemTimer1.h"
#include "Board_LED_1.h"
#include "BitIoLdd1.h"
#include "PTE.h"
#include "CAN1.h"
#include "TU1.h"
#include "PWM_Pump_A.h"
#include "TD1.h"
#include "TU2.h"
#include "TU3.h"
#include "TU4.h"
#include "Board_LED_2.h"
#include "BitIoLdd2.h"
#include "PWM_Pump_B.h"
#include "BitIoLdd3.h"
#include "CH_A.h"
#include "BitIoLdd4.h"
#include "CH_B.h"
#include "BitIoLdd5.h"
#include "B2_SBrush.h"
#include "BitIoLdd6.h"
#include "B3_Vacuum.h"
#include "BitIoLdd7.h"
#include "Power_Button_In.h"
#include "ExtIntLdd2.h"
#include "Contactor_Ctrl.h"
#include "BitIoLdd8.h"
#include "Lsds2.h"
#include "BitIoLdd9.h"
#include "Panel_Ind_R.h"
#include "BitIoLdd10.h"
#include "Panel_Ind_G.h"
#include "BitIoLdd11.h"
#include "Panel_Pwr_LED.h"
#include "BitIoLdd12.h"
#include "TU5.h"
#include "WarningTimer.h"
#include "Panel_Drive_Sw.h"
#include "BitIoLdd13.h"
#include "CsIO1.h"
#include "IO1.h"
#include "Brakes_Ctrl.h"
#include "BitIoLdd14.h"
#include "Lift_measure.h"
#include "AdcLdd1.h"
#include "EStop_In.h"
#include "BitIoLdd15.h"
#include "Left_LED.h"
#include "PwmLdd2.h"
#include "R_StatusStrip.h"
#include "PwmLdd3.h"
#include "G_StatusStrip.h"
#include "PwmLdd4.h"
#include "B_StatusStrip.h"
#include "PwmLdd5.h"
#include "CH_EN.h"
#include "BitIoLdd16.h"
#include "Pump_EN.h"
#include "BitIoLdd17.h"
#include "PwmLdd1.h"
#include "TU6.h"
#include "Right_LED.h"
#include "global_variables.h"
#ifdef __cplusplus
extern "C" {
#endif 

/*
** ===================================================================
**     Event       :  Cpu_OnNMIINT (module Events)
**
**     Component   :  Cpu [MK20N512LQ100]
*/
/*!
**     @brief
**         This event is called when the Non maskable interrupt had
**         occurred. This event is automatically enabled when the [NMI
**         interrupt] property is set to 'Enabled'.
*/
/* ===================================================================*/
void Cpu_OnNMIINT(void);

/*
** ===================================================================
**     Event       :  CAN1_OnFreeTxBuffer (module Events)
**
**     Component   :  CAN1 [CAN_LDD]
*/
/*!
**     @brief
**         This event is called when the buffer is empty after a
**         successful transmit of a message. This event is available
**         only if method SendFrame is enabled.
**     @param
**         UserDataPtr     - Pointer to the user or
**                           RTOS specific data. This pointer is passed
**                           as the parameter of Init method.
**     @param
**         BufferIdx       - Receive message buffer index.
*/
/* ===================================================================*/
void CAN1_OnFreeTxBuffer(LDD_TUserData *UserDataPtr, LDD_CAN_TMBIndex BufferIdx);

/*
** ===================================================================
**     Event       :  CAN1_OnFullRxBuffer (module Events)
**
**     Component   :  CAN1 [CAN_LDD]
*/
/*!
**     @brief
**         This event is called when the buffer is full after a
**         successful receive a message. This event is available only
**         if method ReadFrame or SetRxBufferState is enabled.
**     @param
**         UserDataPtr     - Pointer to the user or
**                           RTOS specific data. This pointer is passed
**                           as the parameter of Init method.
**     @param
**         BufferIdx       - Transmit buffer index.
*/
/* ===================================================================*/
void CAN1_OnFullRxBuffer(LDD_TUserData *UserDataPtr, LDD_CAN_TMBIndex BufferIdx);

/*
** ===================================================================
**     Event       :  TU4_OnCounterRestart (module Events)
**
**     Component   :  TU4 [TimerUnit_LDD]
*/
/*!
**     @brief
**         Called if counter overflow/underflow or counter is
**         reinitialized by modulo or compare register matching.
**         OnCounterRestart event and Timer unit must be enabled. See
**         [SetEventMask] and [GetEventMask] methods. This event is
**         available only if a [Interrupt] is enabled.
**     @param
**         UserDataPtr     - Pointer to the user or
**                           RTOS specific data. The pointer passed as
**                           the parameter of Init method.
*/
/* ===================================================================*/
void TU4_OnCounterRestart(LDD_TUserData *UserDataPtr);

/*
** ===================================================================
**     Event       :  TU3_OnCounterRestart (module Events)
**
**     Component   :  TU3 [TimerUnit_LDD]
*/
/*!
**     @brief
**         Called if counter overflow/underflow or counter is
**         reinitialized by modulo or compare register matching.
**         OnCounterRestart event and Timer unit must be enabled. See
**         [SetEventMask] and [GetEventMask] methods. This event is
**         available only if a [Interrupt] is enabled.
**     @param
**         UserDataPtr     - Pointer to the user or
**                           RTOS specific data. The pointer passed as
**                           the parameter of Init method.
*/
/* ===================================================================*/
void TU3_OnCounterRestart(LDD_TUserData *UserDataPtr);

void Power_Button_In_OnInterrupt(void);
/*
** ===================================================================
**     Event       :  Power_Button_In_OnInterrupt (module Events)
**
**     Component   :  Power_Button_In [ExtInt]
**     Description :
**         This event is called when an active signal edge/level has
**         occurred.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

/*
** ===================================================================
**     Event       :  WarningTimer_OnCounterRestart (module Events)
**
**     Component   :  WarningTimer [TimerUnit_LDD]
*/
/*!
**     @brief
**         Called if counter overflow/underflow or counter is
**         reinitialized by modulo or compare register matching.
**         OnCounterRestart event and Timer unit must be enabled. See
**         [SetEventMask] and [GetEventMask] methods. This event is
**         available only if a [Interrupt] is enabled.
**     @param
**         UserDataPtr     - Pointer to the user or
**                           RTOS specific data. The pointer passed as
**                           the parameter of Init method.
*/
/* ===================================================================*/
void WarningTimer_OnCounterRestart(LDD_TUserData *UserDataPtr);

/*
** ===================================================================
**     Event       :  TU5_OnCounterRestart (module Events)
**
**     Component   :  TU5 [TimerUnit_LDD]
*/
/*!
**     @brief
**         Called if counter overflow/underflow or counter is
**         reinitialized by modulo or compare register matching.
**         OnCounterRestart event and Timer unit must be enabled. See
**         [SetEventMask] and [GetEventMask] methods. This event is
**         available only if a [Interrupt] is enabled.
**     @param
**         UserDataPtr     - Pointer to the user or
**                           RTOS specific data. The pointer passed as
**                           the parameter of Init method.
*/
/* ===================================================================*/
void TU5_OnCounterRestart(LDD_TUserData *UserDataPtr);

void Lift_measure_OnEnd(void);
/*
** ===================================================================
**     Event       :  Lift_measure_OnEnd (module Events)
**
**     Component   :  Lift_measure [ADC]
**     Description :
**         This event is called after the measurement (which consists
**         of <1 or more conversions>) is/are finished.
**         The event is available only when the <Interrupt
**         service/event> property is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

void Lift_measure_OnCalibrationEnd(void);
/*
** ===================================================================
**     Event       :  Lift_measure_OnCalibrationEnd (module Events)
**
**     Component   :  Lift_measure [ADC]
**     Description :
**         This event is called when the calibration has been finished.
**         User should check if the calibration pass or fail by
**         Calibration status method./nThis event is enabled only if
**         the <Interrupt service/event> property is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/

/* END Events */

#ifdef __cplusplus
}  /* extern "C" */
#endif 

#endif 
/* ifndef __Events_H*/
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.5 [05.21]
**     for the Freescale Kinetis series of microcontrollers.
**
** ###################################################################
*/
