/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2015, Avidbots Corp.
 * @name	avidbots_shiny_can_open.c
 * @brief	Source file that defines the Avidbots shiny can_open functions
 * @author	Pablo Molina
 */

#include "avidbots_shiny_can_open_constants.h"
#include "avidbots_shiny_can_open.h"
#include "can_open_constants.h"
#include "global_variables.h"
#include "can_open.h"
#include "motor_ctrl_pwm.h"

// Local includes from PE

#include "B2_SBrush.h"
#include "CH_A.h"
#include "CH_B.h"
#include "helper_functions.h"
/**
  * @name 	ParseCANOpenMsg
  * @brief	This function parses the can open message and returns 
 			the data msg and the object dictionary 
  * @param[in] 		can_msg:  the CAN msg pointer
  * @param[out] 	data_msg: the data msg pointer
  * @param[out] 	od_index_b1: the object dictionary index byte 1
  * @param[out] 	od_index_b2: the object dictionary index byte 2
  * @param[out] 	sub_index: the object dictionary sub-index  
 */
void ParseCANOpenMsg(uint8_t* can_msg, uint8_t* data_msg, 
						uint8_t* od_index_b1, uint8_t* od_index_b2, 
						uint8_t* sub_index)
{
	//	parsing first byte of object index
	*od_index_b1 	= (uint8_t) can_msg[2];
	//	parsing second byte of object index
	*od_index_b2 	= (uint8_t) can_msg[1];
	//	parsing sub index
	*sub_index 		= (uint8_t) can_msg[3];
	//	parsing command data
	for (uint8_t i = 0 ; i < 4 ; i++)
	{
		data_msg[i] = (uint8_t) can_msg[4+i];
	}
}
/**
  * @name 	ProcessSideBrushSetCmd
  * @brief	Sets the side brushes to the given value. Returns an ACK message  			
  * @param[in] 	data:  the data provided  
*/
void ProcessSideBrushSetCmd(uint8_t* data)
{
	// 	copy data into main buffer
	side_brush_status = (bool) data[0];
	//	set side brush
	B2_SBrush_PutVal(side_brush_status);
	uint16_t can_id = kSDOSend + CAN_ID;
	// 	reply with ACK
	SendSDOCanOpenCAN1(can_id, kSDOWriteMessageByte1,
						kBrushCmdB1,kBrushCmdB2, kSideBrushSubIndex, data);
}

/**
  * @name 	ProcessVacuumSetCmd
  * @brief	Sets the vacuum to the given value. Returns an ACK message  			
  * @param[in] 	data:  the data provided  
*/
void ProcessVacuumSetCmd(uint8_t* data)
{
	// 	copy data into vacuum main buffer
	vacuum_status = (bool) data[0];
	//	set Vacuum
	B3_Vacuum_PutVal(vacuum_status);
	uint16_t can_id = kSDOSend + CAN_ID;
	// 	reply with ACK
	SendSDOCanOpenCAN1(can_id, kSDOWriteMessageByte1,
						kVacuumCmdB1,kVacuumCmdB2, kVacuumSubIndex, data);
}

/**
  * @name 	ProcessCleaningHeadSetCmd
  * @brief	Sets the cleaning head set to the given position. Returns an ACK message
  * @param[in] 	data:  the data provided  
*/
void ProcessCleaningHeadSetCmd(uint8_t* data)
{

	//	local variables of setting cleaning head position
	uint_8 local_motor_position_min = 0;
	uint_8 local_motor_position_max = 0;
	uint_8 local_motor_position = 0;
	uint8_t lift_measure_flag = FALSE;

	// 	copy data into Cleaning head main buffer
	cleaning_head_status = data[0];

	//	go to requested position
	switch(cleaning_head_status)
	{
	//	all the way up
		case 0:
			CH_A_ClrVal();
			CH_B_SetVal();
			printf("Position 0");
			break;
/*	//	down to lowest position
		case 1:
			local_motor_position_max = MOTOR_POSITION_1 + MOTOR_POSITION_TOLERANCE;
			local_motor_position_min = MOTOR_POSITION_1 - MOTOR_POSITION_TOLERANCE;
			local_motor_position = MOTOR_POSITION_1;
			lift_measure_flag = TRUE;
			printf("Position 1");
			break;
	//	middle position
		case 2:
			local_motor_position_max = MOTOR_POSITION_2 + MOTOR_POSITION_TOLERANCE;
			local_motor_position_min = MOTOR_POSITION_2 - MOTOR_POSITION_TOLERANCE;
			local_motor_position = MOTOR_POSITION_2;
			lift_measure_flag = TRUE;
			printf("Position 2");
			break;
	//	position slightly lower than position 0
		case 3:
			local_motor_position_max = MOTOR_POSITION_3 + MOTOR_POSITION_TOLERANCE;
			local_motor_position_min = MOTOR_POSITION_3 - MOTOR_POSITION_TOLERANCE;
			local_motor_position = MOTOR_POSITION_3;
			lift_measure_flag = TRUE;
			printf("Position 3");
			break;
			*/
		default:
			local_motor_position_max = cleaning_head_status + MOTOR_POSITION_TOLERANCE;
			local_motor_position_min = cleaning_head_status - MOTOR_POSITION_TOLERANCE;
			local_motor_position = cleaning_head_status;
			lift_measure_flag = TRUE;
			printf("Position %d\n", cleaning_head_status);

	}
	//	write local variables into glocal variables
	_mutex_lock(&g_t_mutex);
	g_t_motor_position_max = local_motor_position_max;
	g_t_motor_position_min = local_motor_position_min;
	g_t_motor_position = local_motor_position;
	g_t_Lift_measure_flag = lift_measure_flag;
	_mutex_unlock(&g_t_mutex);

	uint16_t can_id = kSDOSend + CAN_ID;
	// 	reply with ACK
	SendSDOCanOpenCAN1(can_id, kSDOWriteMessageByte1,
						kCleaningHeadCmdB1,kCleaningHeadCmdB2, kCleaningHeadSubIndex, data);
}

/**
  * @name 	ProcessCleaningModeSetCmd
  * @brief	Sets the cleaning mode set to the given mode. Returns an ACK message
  * @param[in] 	data:  the data provided  
*/
void ProcessCleaningModeSetCmd(uint8_t* data)
{
	// 	copy data into main buffer
	mode_status = (bool) data[0];
	//	Manual mode
	if(mode_status == TRUE)
	{
		_mutex_lock(&g_t_mutex);
		g_t_state_driving_status = TRUE;
		_mutex_unlock(&g_t_mutex);

	}
	//	Not in Manual mode
	else
	{
		_mutex_lock(&g_t_mutex);
		g_t_state_driving_status = FALSE;
		_mutex_unlock(&g_t_mutex);
	}

	uint16_t can_id = kSDOSend + CAN_ID;
	// 	reply with ACK
	SendSDOCanOpenCAN1(can_id, kSDOWriteMessageByte1,
						kCleaningModeCmdB1,kCleaningModeCmdB2, kCleaningModeSubIndex, data);
}

/**
  * @name 	ProcessPumpSpeedSetCmd
  * @brief	Sets the pump speed to the given value. Returns an ACK message  			
  * @param[in] 	data:  the data provided  
*/
void ProcessPumpSpeedSetCmd(uint8_t* data)
{
	// 	copy data into speed buffer
	pump_speed = data[0];
	// 	Set the PWm speed for pump
	SetPWMPumpValue(pump_speed);
	// 	reply with ACK
	uint16_t can_id = kSDOSend + CAN_ID;
	SendSDOCanOpenCAN1(can_id, kSDOWriteMessageByte1,
						kPumpSpeedCmdB1,kPumpSpeedCmdB2, kPumpSpeedSubIndex, data);
}

/**
  * @name 	ProcessStatusVariableGetCmd
  * @brief	Gets the status variable. Returns an ACK message with the data
*/
void ProcessStatusVariableGetCmd(uint8_t* data)
{
	uint8_t local_state_variable;
	uint8_t local_estop_variable;
	uint8_t local_driving_variable;
	uint8_t status_variable;

	//	Copy state variable in to local buffer
	_mutex_lock(&g_t_mutex);
	local_state_variable = g_t_state_variable;
	//	Copy estop state variable in to local buffer
	local_estop_variable = g_t_estop_status;
	//	Copy driving switch variable in to local buffer
	g_t_driving_sw_status = !Panel_Drive_Sw_GetVal();
	local_driving_variable = g_t_driving_sw_status;
	_mutex_unlock(&g_t_mutex);

	//	Merge three different variables into one byte variable
	//	Bit7 ~ Bit 6: Estop state
	//	Bit5 		: Driving switch status
	// 	Bit4 ~ Bit 0: State variable
	status_variable = (((local_estop_variable & 3) << 6) | ((local_driving_variable & 1) << 5) | (local_state_variable & 0x1F));

	uint16_t can_id = kSDOSend + CAN_ID;
	// 	reply with ACK
	SendSDOCanOpenCAN1(can_id, kSDOReadMessageByte1,
			kReadStatusVariableB1,kReadStatusVariableB2,
			kReadStatusVarSubIndex, &status_variable);
}

/**
  * @name 	ProcessSafetyMonitorSetCmd
  * @brief	Gets the safety status from safety monitor. Returns an ACK message with the data
*/
void ProcessSafetyMonitorSetCmd(uint8_t* data)
{
	LDD_TError Error;
	//	Copy data into safety command buffer
	_mutex_lock(&g_t_mutex);
	g_t_SafetyCmd = data[0];
	_mutex_unlock(&g_t_mutex);
	//	Get current time in hundreds of sec
	Error = TD1_GetTime(CANTimer_device_data_ptr, &current_time);
	timeMS = current_time.Hour * 360000 + current_time.Min*6000 + current_time.Sec*100 + current_time.Sec100;

  	uint16_t can_id = kSDOSend + CAN_ID;
  	// 	reply with ACK
  	SendSDOCanOpenCAN1(can_id, kSDOWriteMessageByte1,
						kWritePCStatusB1,kWritePCStatusB2,
						kWritePCStatusSubIndex, &data[0]);

 }

/**
  * @name 	ProcessEstopReleaseCmd
  * @brief	Gets command of releasing estop. Returns an ACK message with the data
*/
void ProcessEstopReleaseCmd(uint8_t* data)
{
	//	Copy data into local estop command
	uint8_t local_estopCmd = data[0];
	bool local_state_var = MCU_STATE_VAR_OFF;

	_mutex_lock(&g_t_mutex);
	local_state_var = g_t_state_variable;
	_mutex_unlock(&g_t_mutex);
	//	Update estop state
	if(local_state_var == MCU_STATE_VAR_EMERGENCY)
  	{
		_mutex_lock(&g_t_mutex);
		g_t_estop_state = local_estopCmd;
		_mutex_unlock(&g_t_mutex);
  	}

	uint16_t can_id = kSDOSend + CAN_ID;
	// 	reply with ACK
	SendSDOCanOpenCAN1(can_id, kSDOWriteMessageByte1,
						kWriteEStopStateB1,kWriteEStopStateB2,
						kWriteEStopStatSubIndex, &local_estopCmd);
}

