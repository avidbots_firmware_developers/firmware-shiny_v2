/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2015, Avidbots Corp.
 * @name	can_open
 * @brief	Header file that defines the can_open functions
 * @author	Pablo Molina
 */


#ifndef CAN_OPEN_H_
#define CAN_OPEN_H_
#include <stdint.h>
#include "CAN1.h"

extern int can1_user_data;
extern CAN1_TDeviceData *CAN1_device_data_ptr;
extern LDD_TDeviceData *CANTimer_device_data_ptr;

void InitCAN1Timer();

void InitCAN1();

void SendSDOCanOpenCAN1(uint16_t can_id, uint8_t ccs_byte,
											uint8_t od_index_b1, uint8_t od_index_b2,
											uint8_t od_sub_index, uint8_t * msg_data);

void SendHeartbeatCAN1(uint8_t can_id);
void SendUnknowcmdCAN1(uint8_t can_id, uint8_t *data);

#endif /* CAN_OPEN_H_ */
