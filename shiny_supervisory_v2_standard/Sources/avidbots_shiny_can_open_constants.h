/*
 *  ______                   __  __              __
 * /\  _  \           __    /\ \/\ \            /\ \__
 * \ \ \L\ \  __  __ /\_\   \_\ \ \ \____    ___\ \ ,_\   ____
 *  \ \  __ \/\ \/\ \\/\ \  /'_` \ \ '__`\  / __`\ \ \/  /',__\
 *   \ \ \/\ \ \ \_/ |\ \ \/\ \L\ \ \ \L\ \/\ \L\ \ \ \_/\__, `\
 *    \ \_\ \_\ \___/  \ \_\ \___,_\ \_,__/\ \____/\ \__\/\____/
 *     \/_/\/_/\/__/    \/_/\/__,_ /\/___/  \/___/  \/__/\/___/
 * Copyright 2015, Avidbots Corp.
 * @name	avidbtos_shiny_can_open_constants.h
 * @brief	Header file that defines the Avidbots shiny can_open constants
 * @author	Pablo Molina
 */


#ifndef AVIDBOTS_SHINY_CAN_OPEN_CONSTANTS_H_
#define AVIDBOTS_SHINY_CAN_OPEN_CONSTANTS_H_
//  CAN Communication health Heartbeat
#define  	kHeartBeat					0x0A
//  Brushes
#define 	kBrushCmdB1 				0x20
#define 	kBrushCmdB2 				0x01
//  Main Brush
#define 	kMainBrushSubIndex			0x01
#define		kMainBrushCmdOB  			0x200101
//  Side Brush
#define		kSideBrushSubIndex			0x02
#define		kSideBrushCmdOB  			0x200102
//	Vacuum
#define 	kVacuumCmdB1 				0x20
#define 	kVacuumCmdB2 				0x02
#define		kVacuumSubIndex				0x01
#define		kVacuumCmdOB  				0x200201
//	Cleaning head (Lift motor)
#define 	kCleaningHeadCmdB1			0x20
#define 	kCleaningHeadCmdB2			0x03
#define		kCleaningHeadSubIndex		0x01
#define		kCleaningHeadCmdOB  		0x200301
//	Cleaning mode
#define 	kCleaningModeCmdB1			0x20
#define 	kCleaningModeCmdB2			0x04
#define		kCleaningModeSubIndex		0x01
#define		kCleaningModeCmdOB  		0x200401
//	Pump
#define 	kPumpSpeedCmdB1				0x20
#define 	kPumpSpeedCmdB2				0x05
#define 	kPumpSpeedSubIndex			0x01
#define		kPumpSpeedCmdOB  			0x200501
//	status variables (include estop status, driving swtich status, and state variable)
#define 	kReadStatusVariableB1		0x20
#define 	kReadStatusVariableB2		0x0A
#define 	kReadStatusVarSubIndex		0x01
#define		kReadStatusVariableOB       0x200A01
//	Safty monitor
#define 	kWritePCStatusB1			0x20
#define		kWritePCStatusB2			0x0E
#define 	kWritePCStatusSubIndex 		0x01
#define 	kSafetyMonitorSetCmdOB		0x200E01
//	Release estop command
#define 	kWriteEStopStateB1			0x20
#define 	kWriteEStopStateB2			0x0D
#define 	kWriteEStopStatSubIndex		0x01
#define		kEstopSetCmdOB  			0x200D01

#endif /* AVIDBOTS_SHINY_CAN_OPEN_CONSTANTS_H_ */
