################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Generated_Code/AdcLdd1.c \
../Generated_Code/B2_SBrush.c \
../Generated_Code/B3_Vacuum.c \
../Generated_Code/B_StatusStrip.c \
../Generated_Code/BitIoLdd1.c \
../Generated_Code/BitIoLdd10.c \
../Generated_Code/BitIoLdd11.c \
../Generated_Code/BitIoLdd12.c \
../Generated_Code/BitIoLdd13.c \
../Generated_Code/BitIoLdd14.c \
../Generated_Code/BitIoLdd15.c \
../Generated_Code/BitIoLdd16.c \
../Generated_Code/BitIoLdd17.c \
../Generated_Code/BitIoLdd2.c \
../Generated_Code/BitIoLdd3.c \
../Generated_Code/BitIoLdd4.c \
../Generated_Code/BitIoLdd5.c \
../Generated_Code/BitIoLdd6.c \
../Generated_Code/BitIoLdd7.c \
../Generated_Code/BitIoLdd8.c \
../Generated_Code/BitIoLdd9.c \
../Generated_Code/Board_LED_1.c \
../Generated_Code/Board_LED_2.c \
../Generated_Code/Brakes_Ctrl.c \
../Generated_Code/CAN1.c \
../Generated_Code/CH_A.c \
../Generated_Code/CH_B.c \
../Generated_Code/CH_EN.c \
../Generated_Code/Contactor_Ctrl.c \
../Generated_Code/Cpu.c \
../Generated_Code/CsIO1.c \
../Generated_Code/EStop_In.c \
../Generated_Code/ExtIntLdd2.c \
../Generated_Code/G_StatusStrip.c \
../Generated_Code/IO1.c \
../Generated_Code/Left_LED.c \
../Generated_Code/Lift_measure.c \
../Generated_Code/Lsds2.c \
../Generated_Code/MQX1.c \
../Generated_Code/PE_LDD.c \
../Generated_Code/PTE.c \
../Generated_Code/PWM_Pump_A.c \
../Generated_Code/PWM_Pump_B.c \
../Generated_Code/Panel_Drive_Sw.c \
../Generated_Code/Panel_Ind_G.c \
../Generated_Code/Panel_Ind_R.c \
../Generated_Code/Panel_Pwr_LED.c \
../Generated_Code/Power_Button_In.c \
../Generated_Code/Pump_EN.c \
../Generated_Code/PwmLdd1.c \
../Generated_Code/PwmLdd2.c \
../Generated_Code/PwmLdd3.c \
../Generated_Code/PwmLdd4.c \
../Generated_Code/PwmLdd5.c \
../Generated_Code/R_StatusStrip.c \
../Generated_Code/Right_LED.c \
../Generated_Code/SystemTimer1.c \
../Generated_Code/TD1.c \
../Generated_Code/TU1.c \
../Generated_Code/TU2.c \
../Generated_Code/TU3.c \
../Generated_Code/TU4.c \
../Generated_Code/TU5.c \
../Generated_Code/TU6.c \
../Generated_Code/Vectors.c \
../Generated_Code/WarningTimer.c 

OBJS += \
./Generated_Code/AdcLdd1.o \
./Generated_Code/B2_SBrush.o \
./Generated_Code/B3_Vacuum.o \
./Generated_Code/B_StatusStrip.o \
./Generated_Code/BitIoLdd1.o \
./Generated_Code/BitIoLdd10.o \
./Generated_Code/BitIoLdd11.o \
./Generated_Code/BitIoLdd12.o \
./Generated_Code/BitIoLdd13.o \
./Generated_Code/BitIoLdd14.o \
./Generated_Code/BitIoLdd15.o \
./Generated_Code/BitIoLdd16.o \
./Generated_Code/BitIoLdd17.o \
./Generated_Code/BitIoLdd2.o \
./Generated_Code/BitIoLdd3.o \
./Generated_Code/BitIoLdd4.o \
./Generated_Code/BitIoLdd5.o \
./Generated_Code/BitIoLdd6.o \
./Generated_Code/BitIoLdd7.o \
./Generated_Code/BitIoLdd8.o \
./Generated_Code/BitIoLdd9.o \
./Generated_Code/Board_LED_1.o \
./Generated_Code/Board_LED_2.o \
./Generated_Code/Brakes_Ctrl.o \
./Generated_Code/CAN1.o \
./Generated_Code/CH_A.o \
./Generated_Code/CH_B.o \
./Generated_Code/CH_EN.o \
./Generated_Code/Contactor_Ctrl.o \
./Generated_Code/Cpu.o \
./Generated_Code/CsIO1.o \
./Generated_Code/EStop_In.o \
./Generated_Code/ExtIntLdd2.o \
./Generated_Code/G_StatusStrip.o \
./Generated_Code/IO1.o \
./Generated_Code/Left_LED.o \
./Generated_Code/Lift_measure.o \
./Generated_Code/Lsds2.o \
./Generated_Code/MQX1.o \
./Generated_Code/PE_LDD.o \
./Generated_Code/PTE.o \
./Generated_Code/PWM_Pump_A.o \
./Generated_Code/PWM_Pump_B.o \
./Generated_Code/Panel_Drive_Sw.o \
./Generated_Code/Panel_Ind_G.o \
./Generated_Code/Panel_Ind_R.o \
./Generated_Code/Panel_Pwr_LED.o \
./Generated_Code/Power_Button_In.o \
./Generated_Code/Pump_EN.o \
./Generated_Code/PwmLdd1.o \
./Generated_Code/PwmLdd2.o \
./Generated_Code/PwmLdd3.o \
./Generated_Code/PwmLdd4.o \
./Generated_Code/PwmLdd5.o \
./Generated_Code/R_StatusStrip.o \
./Generated_Code/Right_LED.o \
./Generated_Code/SystemTimer1.o \
./Generated_Code/TD1.o \
./Generated_Code/TU1.o \
./Generated_Code/TU2.o \
./Generated_Code/TU3.o \
./Generated_Code/TU4.o \
./Generated_Code/TU5.o \
./Generated_Code/TU6.o \
./Generated_Code/Vectors.o \
./Generated_Code/WarningTimer.o 

C_DEPS += \
./Generated_Code/AdcLdd1.d \
./Generated_Code/B2_SBrush.d \
./Generated_Code/B3_Vacuum.d \
./Generated_Code/B_StatusStrip.d \
./Generated_Code/BitIoLdd1.d \
./Generated_Code/BitIoLdd10.d \
./Generated_Code/BitIoLdd11.d \
./Generated_Code/BitIoLdd12.d \
./Generated_Code/BitIoLdd13.d \
./Generated_Code/BitIoLdd14.d \
./Generated_Code/BitIoLdd15.d \
./Generated_Code/BitIoLdd16.d \
./Generated_Code/BitIoLdd17.d \
./Generated_Code/BitIoLdd2.d \
./Generated_Code/BitIoLdd3.d \
./Generated_Code/BitIoLdd4.d \
./Generated_Code/BitIoLdd5.d \
./Generated_Code/BitIoLdd6.d \
./Generated_Code/BitIoLdd7.d \
./Generated_Code/BitIoLdd8.d \
./Generated_Code/BitIoLdd9.d \
./Generated_Code/Board_LED_1.d \
./Generated_Code/Board_LED_2.d \
./Generated_Code/Brakes_Ctrl.d \
./Generated_Code/CAN1.d \
./Generated_Code/CH_A.d \
./Generated_Code/CH_B.d \
./Generated_Code/CH_EN.d \
./Generated_Code/Contactor_Ctrl.d \
./Generated_Code/Cpu.d \
./Generated_Code/CsIO1.d \
./Generated_Code/EStop_In.d \
./Generated_Code/ExtIntLdd2.d \
./Generated_Code/G_StatusStrip.d \
./Generated_Code/IO1.d \
./Generated_Code/Left_LED.d \
./Generated_Code/Lift_measure.d \
./Generated_Code/Lsds2.d \
./Generated_Code/MQX1.d \
./Generated_Code/PE_LDD.d \
./Generated_Code/PTE.d \
./Generated_Code/PWM_Pump_A.d \
./Generated_Code/PWM_Pump_B.d \
./Generated_Code/Panel_Drive_Sw.d \
./Generated_Code/Panel_Ind_G.d \
./Generated_Code/Panel_Ind_R.d \
./Generated_Code/Panel_Pwr_LED.d \
./Generated_Code/Power_Button_In.d \
./Generated_Code/Pump_EN.d \
./Generated_Code/PwmLdd1.d \
./Generated_Code/PwmLdd2.d \
./Generated_Code/PwmLdd3.d \
./Generated_Code/PwmLdd4.d \
./Generated_Code/PwmLdd5.d \
./Generated_Code/R_StatusStrip.d \
./Generated_Code/Right_LED.d \
./Generated_Code/SystemTimer1.d \
./Generated_Code/TD1.d \
./Generated_Code/TU1.d \
./Generated_Code/TU2.d \
./Generated_Code/TU3.d \
./Generated_Code/TU4.d \
./Generated_Code/TU5.d \
./Generated_Code/TU6.d \
./Generated_Code/Vectors.d \
./Generated_Code/WarningTimer.d 


# Each subdirectory must supply rules for building sources it contributes
Generated_Code/%.o: ../Generated_Code/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/Static_Code/PDD" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/Static_Code/IO_Map" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/Sources" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/Generated_Code" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/MQXLITE/include" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/MQXLITE/config" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/MQXLITE/kernel" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/MQXLITE/psp/cortex_m" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/MQXLITE/psp/cortex_m/core/M4" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/MQXLITE/psp/cortex_m/compiler/cwgcc" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


