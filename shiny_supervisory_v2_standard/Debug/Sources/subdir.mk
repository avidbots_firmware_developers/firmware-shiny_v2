################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Sources/Events.c \
../Sources/avidbots_shiny_can_open.c \
../Sources/can_open.c \
../Sources/comm_task_main.c \
../Sources/control_task_main.c \
../Sources/global_variables.c \
../Sources/helper_functions.c \
../Sources/main.c \
../Sources/motor_ctrl_pwm.c \
../Sources/mqx_tasks.c \
../Sources/safety_task_main.c \
../Sources/state_task_main.c 

OBJS += \
./Sources/Events.o \
./Sources/avidbots_shiny_can_open.o \
./Sources/can_open.o \
./Sources/comm_task_main.o \
./Sources/control_task_main.o \
./Sources/global_variables.o \
./Sources/helper_functions.o \
./Sources/main.o \
./Sources/motor_ctrl_pwm.o \
./Sources/mqx_tasks.o \
./Sources/safety_task_main.o \
./Sources/state_task_main.o 

C_DEPS += \
./Sources/Events.d \
./Sources/avidbots_shiny_can_open.d \
./Sources/can_open.d \
./Sources/comm_task_main.d \
./Sources/control_task_main.d \
./Sources/global_variables.d \
./Sources/helper_functions.d \
./Sources/main.d \
./Sources/motor_ctrl_pwm.d \
./Sources/mqx_tasks.d \
./Sources/safety_task_main.d \
./Sources/state_task_main.d 


# Each subdirectory must supply rules for building sources it contributes
Sources/%.o: ../Sources/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/Static_Code/PDD" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/Static_Code/IO_Map" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/Sources" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/Generated_Code" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/MQXLITE/include" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/MQXLITE/config" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/MQXLITE/kernel" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/MQXLITE/psp/cortex_m" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/MQXLITE/psp/cortex_m/core/M4" -I"D:/Avidbots/Shiny_firmware_v2/shiny_supervisory_v2_standard/MQXLITE/psp/cortex_m/compiler/cwgcc" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


